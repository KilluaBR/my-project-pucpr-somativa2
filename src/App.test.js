import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react';
import App from './App';

test('App Component', () => {
  const { getByText, getByPlaceholderText } = render(<App />);

  // Verifica se o texto "List of Tasks" está presente no componente
  const titleElement = getByText(/List of Tasks/i);
  expect(titleElement).toBeInTheDocument();

  // Verifica se o campo de entrada e o botão estão presentes
  const inputElement = getByPlaceholderText('add Task');
  const addButtonElement = getByText('Add Task');
  expect(inputElement).toBeInTheDocument();
  expect(addButtonElement).toBeInTheDocument();
});

test('Adicionar Nova Task', async () => {
  const { getByPlaceholderText, getByText, getByTestId } = render(<App />);

  // Encontra o campo de entrada e o botão
  const inputElement = getByPlaceholderText('add Task');
  const addButtonElement = getByText('Add Task');

  // Insere um texto no campo de entrada
  fireEvent.change(inputElement, { target: { value: 'New Task' } });

  // Clica no botão "Add Task"
  fireEvent.click(addButtonElement);

  // Inclua instruções de depuração para verificar o estado
  console.log('newTasks value:', inputElement.value);
  console.log('tasks:', JSON.stringify(getByTestId('task-0')));

  // Aguarda até que o elemento seja renderizado
  await waitFor(() => {
    const taskElement = getByTestId('task-0');
    expect(taskElement).toBeInTheDocument();
    expect(taskElement).toHaveTextContent('New Task');
  });
});

test('completes a task', () => {
  const { getByPlaceholderText, getByText, getByTestId } = render(<App />);

  // Encontra o campo de entrada e o botão
  const inputElement = getByPlaceholderText('add Task');
  const addButtonElement = getByText('Add Task');

  // Insere uma nova tarefa
  fireEvent.change(inputElement, { target: { value: 'New Task' } });
  fireEvent.click(addButtonElement);

  // Encontra a tarefa e completa
  const taskElement = getByTestId('task-0');
  const completeButtonElement = getByText('Completed');
  fireEvent.click(completeButtonElement);

  // Verifica se a tarefa está marcada como concluída
  expect(taskElement).toHaveClass('completed');
});

test('deletes a task', () => {
  const { getByPlaceholderText, getByText, getByTestId, queryByTestId } = render(<App />);

  // Encontra o campo de entrada e o botão
  const inputElement = getByPlaceholderText('add Task');
  const addButtonElement = getByText('Add Task');

  // Insere uma nova tarefa
  fireEvent.change(inputElement, { target: { value: 'New Task' } });
  fireEvent.click(addButtonElement);

  // Encontra a tarefa e a exclui
  const taskElement = getByTestId('task-0');
  const deleteButtonElement = getByText('Deleted');
  fireEvent.click(deleteButtonElement);

  // Verifica se a tarefa foi removida da lista
  const deletedTaskElement = queryByTestId('task-0');
  expect(deletedTaskElement).toBeNull();
});

